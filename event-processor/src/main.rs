use std::error::Error;

use crate::kafka::create_producer;
use dotenvy::dotenv;

mod api;
mod database;
mod kafka;
mod schema;
mod services;

#[actix_web::main]
async fn main() -> Result<(), Box<dyn Error>> {
    std::env::set_var("RUST_LOG", "debug");
    env_logger::init();

    dotenv().ok();

    let db_pool = database::initialize_db_pool();

    let producer = create_producer()?;

    api::routes::create_server(db_pool, producer).await?;

    Ok(())
}
