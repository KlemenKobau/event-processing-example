use std::{
    env,
    sync::{Mutex},
};

use actix_web::{
    error::ErrorInternalServerError,
    web::{self, Data, Path, Query},
    App, HttpResponse, HttpServer, Responder, get,
};
use chrono::Utc;
use kafka::producer::Producer;

use crate::{
    database::DbPool,
    services::{dto::EventRequest, event_processer::process_event},
};

#[get("/{account_id}")]
async fn receive_event(
    pool: Data<DbPool>,
    kafka_producer: Data<Mutex<Producer>>,
    path: Path<String>,
    query: Query<crate::api::models::Data>,
) -> actix_web::Result<impl Responder> {
    let req = EventRequest {
        account_uuid: path.into_inner(),
        timestamp: Utc::now(),
        data: query.0.data,
    };

    web::block(move || {
        let mut conn = pool.get().expect("couldn't get db connection from pool");
        process_event(&mut conn, req, kafka_producer.into_inner())
    })
    .await?
    .map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok())
}

pub async fn create_server(db_poll: DbPool, kafka_producer: Producer) -> std::io::Result<()> {
    let host = env::var("HOST").unwrap_or("localhost".to_owned());
    let port = env::var("PORT")
        .ok()
        .and_then(|x| x.parse().ok())
        .unwrap_or(8080);

    let producer = Data::new(Mutex::new(kafka_producer));

    HttpServer::new(move || {
        App::new()
            .app_data(Data::clone(&producer))
            .app_data(Data::new(db_poll.clone()))
            .service(receive_event)
    })
    .bind((host, port))?
    .run()
    .await
}
