use serde::Deserialize;

#[derive(Deserialize)]
pub struct Data {
    pub data: String,
}
