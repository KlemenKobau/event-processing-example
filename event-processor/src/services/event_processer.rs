use std::{
    error::Error,
    sync::{Arc, Mutex}, env,
};

use diesel::{dsl::exists, select, ExpressionMethods, PgConnection, QueryDsl, RunQueryDsl};
use kafka::producer::{Producer, Record};
use log::{error, info};

use super::dto::EventRequest;

pub fn process_event(
    conn: &mut PgConnection,
    event_request: EventRequest,
    kafka_producer: Arc<Mutex<Producer>>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let account_active = is_account_active(conn, &event_request)?;

    if account_active {
        let topic = env::var("KAFKA_EVENTS_TOPIC").expect("Kafka topic should be defined.");

        let mut producer = kafka_producer
            .lock().expect("Error aquiring lock for producer mutex.");

        let send_res = producer.send(&Record::from_value(
            &topic,
            serde_json::to_string(&event_request)?,
        ));
        if let Err(err) = send_res {
            error!("Error sending message: {}", err);
            return Err(err.into());
        }
    }

    Ok(())
}

fn is_account_active(
    conn: &mut PgConnection,
    event_request: &EventRequest,
) -> Result<bool, diesel::result::Error> {
    use crate::schema::events::dsl::*;

    return select(exists(
        events
            .filter(account_uuid.eq(&event_request.account_uuid))
            .filter(is_active.eq(true)),
    ))
    .get_result::<bool>(conn);
}
