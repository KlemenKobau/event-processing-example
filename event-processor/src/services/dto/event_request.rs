use chrono::{DateTime, Utc};
use serde::Serialize;

#[derive(Serialize)]
pub struct EventRequest {
    pub account_uuid: String,
    pub timestamp: DateTime<Utc>,
    pub data: String,
}
