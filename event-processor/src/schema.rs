// @generated automatically by Diesel CLI.

diesel::table! {
    events (account_id) {
        account_id -> Int4,
        account_uuid -> Varchar,
        account_name -> Varchar,
        is_active -> Bool,
    }
}
