use std::{env, error::Error, time::Duration};

use kafka::producer::{Producer, RequiredAcks};

pub fn create_producer() -> Result<Producer, Box<dyn Error>> {
    let kafka_url = env::var("KAFKA_URL").expect("Kafka URL should be defined.");

    let producer = Producer::from_hosts(vec![kafka_url])
        .with_ack_timeout(Duration::from_secs(1))
        .with_required_acks(RequiredAcks::One)
        .create()?;

    Ok(producer)
}
