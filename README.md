minikube start
minikube addons enable ingress

```bash
helm repo add strimzi https://strimzi.io/charts
helm install kafka-operator strimzi/strimzi-kafka-operator --version 0.35.1 -n kafka-operator --create-namespace -f deployments/kafka-operator.yaml
```

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install postgresql bitnami/postgresql --version 12.6.5 -f deployments/postgres.yaml
kubectl apply -f deployments/kafka.yaml
kubectl apply -f deployments/service-test.yaml
```

v minikube se baje da exposat service

minikube service kafka-cluster-kafka-brokers --url

TODO:
- naredi nov service za kafko in dodaj node port
- podbno naredi za postgres